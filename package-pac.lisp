;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               package.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    See defpackage documentation string.
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2013-11-30 <PJB> cl:package wrapper for LispOS.
;;;;BUGS
;;;;
;;;;    make-load-form for packages should probably return two forms, since
;;;;    packages can have circular dependencies.
;;;;
;;;;    Are missing some standard restarts to correct
;;;;    conflicts. (choosing one or the other symbol, doing the same
;;;;    for all conflicts, etc).
;;;;
;;;;LEGAL
;;;;    AGPL3
;;;;    
;;;;    Copyright Pascal J. Bourguignon 2013 - 2013
;;;;    
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;    
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;    
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************


(cl:defpackage "COM.INFORMATIMAGO.COMMON-LISP.LISPOS.PACKAGE"
  (:use "COMMON-LISP")
  (:shadow "SIMPLE-TYPE-ERROR")
  (:shadow . #1=("SYMBOL"
                 "SYMBOLP" "MAKE-SYMBOL" "SYMBOL-NAME" "SYMBOL-PACKAGE" 
                 "SYMBOL-VALUE" "SYMBOL-FUNCTION" "SYMBOL-PLIST"
                 "BOUNDP" "FBOUNDP"
                 "KEYWORD" "KEYWORDP"
                 "PACKAGE"
                 "PACKAGEP"  "MAKE-PACKAGE" "FIND-PACKAGE" "DELETE-PACKAGE"
                 "FIND-SYMBOL" "IMPORT" "INTERN" "SHADOW" "SHADOWING-IMPORT"
                 "EXPORT" "UNEXPORT" "UNINTERN" "USE-PACKAGE"
                 "UNUSE-PACKAGE" "PACKAGE-NAME" "PACKAGE-NICKNAMES"
                 "PACKAGE-USE-LIST" "PACKAGE-USED-BY-LIST" "PACKAGE-SHADOWING-SYMBOLS"
                 "LIST-ALL-PACKAGES" "FIND-ALL-SYMBOLS" "RENAME-PACKAGE"
                 "*PACKAGE*"
                 "WITH-PACKAGE-ITERATOR"
                 "DO-SYMBOLS" "DO-EXTERNAL-SYMBOLS" "DO-ALL-SYMBOLS"
                 "DEFPACKAGE" "IN-PACKAGE" 
                 "PACKAGE-ERROR" "PACKAGE-ERROR-PACKAGE"))
  (:export . #1#)
  ;; Additionnal conditions:
  (:export "PACKAGE-EXISTS-ERROR"
           "PACKAGE-DOES-NOT-EXIST-ERROR"
           "SYMBOL-CONFLICT-ERROR"
           "SYMBOL-CONFLICT-EXISTING-SYMBOL"
           "SYMBOL-CONFLICT-IMPORTED-SYMBOL"
           "PACKAGE-DOCUMENTATION")
  (:documentation "
This package is a CLOS wrapper over the Common Lisp package system.

It shadows the CL symbols dealing with packages, and exports
replacements generic functions and CLOS objects.


Additionnal symbol exported:

    PACKAGE-EXISTS-ERROR
    PACKAGE-DOES-NOT-EXIST-ERROR
    SYMBOL-CONFLICT-ERROR
    SYMBOL-CONFLICT-EXISTING-SYMBOL
    SYMBOL-CONFLICT-IMPORTED-SYMBOL
    PACKAGE-DOCUMENTATION


License:

    AGPL3
    
    Copyright Pascal J. Bourguignon 2013 - 2013
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"))


;;;; THE END ;;;;
